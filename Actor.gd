extends Area2D

const StateMachine = preload("res://StateMachine.gd")

signal possessed
signal dead
signal said_something

const STATE_DEAD : int = 0
const STATE_IDLE : int = 1
const STATE_WALKING : int = 2
const STATE_SPEAKING : int = 3

var states : Array = [
	preload("res://states/dead.gd").new(self),
	preload("res://states/idle.gd").new(self),
	preload("res://states/walking.gd").new(self),
	preload("res://states/speaking.gd").new(self)
]

const MOVEMENT_SPEED : int = 100

export (Texture) var spritesheet_overwrite

var theatre : Node2D = null
var direction : Vector2 = Vector2.ZERO
var ghost_is_nearby : bool = false

onready var sprite : Sprite = $Sprite
onready var animation_player : AnimationPlayer = $AnimationPlayer
onready var state_machine : StateMachine = $StateMachine

onready var ui : CanvasLayer = $UI
onready var dialogue_option_panel : Panel = $UI/DialogueOptionPanel
onready var dialogue_option_box : VBoxContainer = $UI/DialogueOptionPanel/DialogOptionBox
onready var viewport_size = get_viewport().size


func _ready() -> void:
	if self.spritesheet_overwrite != null:
		self.sprite.texture = self.spritesheet_overwrite

	self.state_machine.init(self.states, STATE_DEAD, $UI/StateDebugLabel)
	
	self.dialogue_option_panel.hide()


func _physics_process(delta: float) -> void:
	# Make the UI follow the actor manually, because
	# CanvasLayers don't follow the Nodes they're attached to
	# by default.
	self.ui.offset = self.global_position + self.viewport_size / 2
	if self.global_position.x > 0:
		self.ui.offset.x -= self.dialogue_option_panel.margin_right + 15


	self.position += self.direction * MOVEMENT_SPEED * delta


func set_direction() -> void:
	var dir : Vector2 = Vector2.ZERO

	if Input.is_action_pressed("move_up"):
		dir += Vector2.UP
	if Input.is_action_pressed("move_down"):
		dir += Vector2.DOWN
	if Input.is_action_pressed("move_left"):
		dir += Vector2.LEFT
	if Input.is_action_pressed("move_right"):
		dir += Vector2.RIGHT

	self.direction = dir.normalized()

	if self.direction.x > 0:
		self.sprite.set_flip_h(false)
	if self.direction.x < 0:
		self.sprite.set_flip_h(true)


func generate_dialogue_option_buttons(option_set : Array) -> void:
	# Clear old buttons
	for btn in self.dialogue_option_box.get_children():
		btn.queue_free()

	# Generate new buttons
	for option in option_set:
		var button : Button = Button.new()
		button.text = option[1]
		button.connect("pressed", self.states[STATE_SPEAKING], "_on_dialogue_option_selected", [option[2], option[0]])
		self.dialogue_option_box.add_child(button)


func set_ghost_is_nearby(is_nearby : bool) -> void:
	self.ghost_is_nearby = is_nearby


func get_state_i() -> int:
	return self.state_machine.current_state_i


func get_state_name() -> String:
	return self.states[self.get_state_i()]._to_string()


func signal_possession() -> void:
	emit_signal("possessed")


func signal_death() -> void:
	emit_signal("dead", self.global_position)


func speak(text : String, portrait_i : int) -> void:
	emit_signal("said_something", text, portrait_i)