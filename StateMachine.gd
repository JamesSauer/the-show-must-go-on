extends Node

var states : Array = []
var state_count : int

var current_state_i : int = -1 setget set_state
var previous_state_i : int = -1

var current_state : BaseState = null
var previous_state : BaseState = null

var debug_label : Label


func init(_states : Array, initial_state : int = 0, _debug_label : Label = null) -> void:
    self.states = _states
    self.state_count = self.states.size()

    self.debug_label = _debug_label

    self.set_state(initial_state)


func _unhandled_input(event: InputEvent) -> void:
    if !is_instance_valid(self.current_state):
        return

    var next_state_i : int = self.current_state._handle_input(event)
    if next_state_i >= 0:
        self.set_state(next_state_i)


func _physics_process(delta: float) -> void:
    if !is_instance_valid(self.current_state):
        return

    var next_state_i : int = self.current_state._handle_physics(delta)
    if next_state_i >= 0:
        self.set_state(next_state_i)


func set_state(next_state_i : int) -> void:
    # Make sure i is not out of range
    if next_state_i < 0 or next_state_i >= self.state_count:
        return

    # Exit current state
    if self.current_state != null:
        self.current_state._exit(next_state_i)

    # Enter next state
    var next_state : BaseState = self.states[next_state_i]
    next_state._enter(self.current_state_i)

    # Set previous state
    self.previous_state_i = self.current_state_i
    self.previous_state = self.current_state

    # Set current state
    current_state_i = next_state_i # Can't use self here because this is used as setter.
    self.current_state = next_state

    # Display state name
    if self.debug_label != null:
        # For reasons, _to_string has to be called explicitly.
        # Casting as String does not work.
        self.debug_label.text = self.current_state._to_string()
        

func _exit_tree() -> void:
    # Without this clean up, exiting the game leaks instances and causes errors.
    for s in self.states:
        s.free()



class BaseState extends Node:
    var entity : Node = null
    var script_name : String
    
    func _init(_entity : Node) -> void:
        self.entity = _entity

        # Get the name of the state script for use as state name
        var regex : RegEx = RegEx.new()
        regex.compile("(\\w+)\\.gd$")
        # self.get_script().get_path().get_file() also works instead of get_script().resource_path,
        # but also has the file extension.
        var reg_match : RegExMatch = regex.search(self.get_script().resource_path)

        self.script_name = reg_match.get_string(1)

    func _to_string() -> String:
        return self.script_name

    # _handle_input handles all state logic that depends on input.
    # It is called in the StateMachine's _unhandled_input() method.
    # It returns the index of the next state to be transitioned into.
    func _handle_input(_input : InputEvent) -> int:
        return -1

    # _handle_physics handles all state logic that depends on phyics.
    # It is called in the StateMachine's _physics_process() method.
    # It optionally returns the index of the next state to be transitioned into.
    func _handle_physics(_delta : float) -> int:
        return -1

    # _enter is executed by the StateMachine, when this state is transitioned into.
    func _enter(_previous_state : int) -> void:
        pass

    # _exit is executed by the StateMachine, when this state is transitioned out of.
    func _exit(_next_state : int) -> void:
        pass
    
