extends "res://StateMachine.gd".BaseState


func _init(_entity : Node).(_entity) -> void:
    pass


func _handle_input(input : InputEvent) -> int:
    if input.is_action_pressed("speak") and self.entity.theatre.actor_in_spotlight == self.entity:
        return self.entity.STATE_SPEAKING

    return -1


func _handle_physics(_delta : float) -> int:
    self.entity.set_direction()

    if self.entity.direction.length() < 0.1:
        return self.entity.STATE_IDLE

    return -1


func _enter(_previous_state : int) -> void:
    self.entity.animation_player.play("walk")


func _exit(_next_state : int) -> void:
    pass