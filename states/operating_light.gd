extends "res://StateMachine.gd".BaseState


func _init(_entity : Node).(_entity) -> void:
    pass


func _handle_input(input : InputEvent) -> int:
    if input.is_action_pressed("possess"):
        return self.entity.STATE_DEAD

    return -1


func _handle_physics(delta : float) -> int:
    var dir : Vector2 = Vector2.ZERO

    if Input.is_action_pressed("move_left"):
        dir += Vector2.LEFT
    if Input.is_action_pressed("move_right"):
        dir += Vector2.RIGHT

    self.entity.spot.position += dir.normalized() * self.entity.SPEED_OF_LIGHT * delta

    return -1


func _enter(_previous_state : int) -> void:
    self.entity.sprite.frame = 1


func _exit(_next_state : int) -> void:
    pass