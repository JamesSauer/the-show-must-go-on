extends "res://StateMachine.gd".BaseState


func _init(_entity : Node).(_entity) -> void:
    pass


func _on_dialogue_option_selected(full_line : String, portrait_i : int) -> void:
    self.entity.speak(full_line, portrait_i)
    self.entity.dialogue_option_panel.hide()


func _handle_input(input : InputEvent) -> int:
    if input.is_action_pressed("possess"):
        return self.entity.STATE_DEAD

    if input.is_action_pressed("speak"):
        return self.entity.STATE_IDLE

    return -1


func _handle_physics(_delta : float) -> int:
    if !self.entity.dialogue_option_panel.visible:
        return self.entity.STATE_IDLE

    return -1


func _enter(_previous_state : int) -> void:
    self.entity.direction = Vector2.ZERO

    var set : Array = self.entity.theatre.get_dialogue_option_set()
    self.entity.generate_dialogue_option_buttons(set)

    self.entity.dialogue_option_panel.show()


func _exit(_next_state : int) -> void:
    self.entity.dialogue_option_panel.hide()