extends "res://StateMachine.gd".BaseState


func _init(_entity : Node).(_entity) -> void:
    pass


func _handle_input(input : InputEvent) -> int:
    if input.is_action_pressed("possess") and self.entity.ghost_is_nearby:
        return self.entity.STATE_OPERATING_LIGHT

    return -1


func _handle_physics(_delta : float) -> int:
    return -1


func _enter(_previous_state : int) -> void:
    self.entity.signal_death()
    self.entity.sprite.frame = 0


func _exit(_next_state : int) -> void:
    self.entity.signal_possession()