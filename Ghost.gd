extends Node2D

const MOVEMENT_SPEED : int = 100

var direction : Vector2 = Vector2.ZERO

onready var sprite : Sprite = $Sprite


func _physics_process(delta: float) -> void:
	self.set_direction()
	self.position += self.direction * MOVEMENT_SPEED * delta


func set_direction() -> void:
	var dir : Vector2 = Vector2.ZERO

	if Input.is_action_pressed("move_up"):
		dir += Vector2.UP
	if Input.is_action_pressed("move_down"):
		dir += Vector2.DOWN
	if Input.is_action_pressed("move_left"):
		dir += Vector2.LEFT
	if Input.is_action_pressed("move_right"):
		dir += Vector2.RIGHT

	self.direction = dir.normalized()

	if self.direction.x > 0:
		self.sprite.set_flip_h(false)
	if self.direction.x < 0:
		self.sprite.set_flip_h(true)



