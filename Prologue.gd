extends Node2D

var index : int = 0
var text_snippets : Array = [
	"""
In an extremely unlikely and tragic chain of events, the entire crew of the Undivine Comedy Theatre lost their lives.
... Just when their performance was about to reach its climactic ending.
	""",
	"""
While the other members immediately started their journey to the afterlife, guided by divine light of course, the lone soul of a humble lighting technician wasn't as fortunate.
	""",
	"""
Left behind in the theatre hall, he decided to use his newfound ghostly abilities to keep the show alive.
	""",
	"A show those lucky enough to witness will likely never forget.",
	"""
W,S,A,D or arrows
--> move

Space (while near or in dead body)
--> toggle possessing

E (while possessing and in spotlight)
--> speak

ESC
--> quit
	"""
]

onready var label = $Label


func _ready() -> void:
	label.text = self.text_snippets[self.index]


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		self.proceed()

	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()


func proceed() -> void:
	self.index += 1

	if self.index >= self.text_snippets.size():
		get_tree().change_scene("res://Theatre.tscn")
		return

	label.text = self.text_snippets[self.index]
