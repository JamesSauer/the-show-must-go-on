extends Node2D

const LightOperator = preload("res://LightOperator.gd")
const Actor = preload("res://Actor.gd")

var actor_in_spotlight : Actor = null
var last_actor_to_speak : Actor = null
var current_dialogue_option_set_i : int = 0

var suspicious_stuff_going_on : bool = false
var block_input : bool = false

var audience_has_reacted : bool = false

onready var ghost : Area2D = $Ghost
onready var light_operator : LightOperator = $LightOperator
onready var actors : Array = [
	$Dante,
	$Beatrice
]
onready var actor_in_spotlight_state_label : Label = $ActorInSpotlightStateLabel
onready var dialogue_box : RichTextLabel = $UI/DialoguePanel/DialogueBox
onready var dialogue_options : Array = self.load_dialogue()

onready var actor_portrait_sprite : Sprite = $UI/ActorPortrait/Sprite

onready var audience_suspicion_timer : Timer = $Audience/SuspicionTimer
onready var audience_excitement_timer : Timer = $Audience/ExcitementTimer
onready var audience_suspicion_meter : TextureProgress = $UI/AudienceSuspicionMeter
onready var audience_excitement_meter : TextureProgress = $UI/AudienceExcitementMeter

onready var end_game_timer : Timer = $EndGame/EndGameTimer
onready var end_game_animation_player : AnimationPlayer = $EndGame/AnimationPlayer

onready var fps_display : Label = $UI/FPSDisplay


func _ready() -> void:
	for actor in self.actors:
		actor.theatre = self
		actor.connect("possessed", self, "_on_actor_possession")
		actor.connect("dead", self, "_on_actor_death")
		actor.connect("said_something", self, "_on_actor_speaking", [actor])

	self.light_operator.connect("possessed", self, "_on_actor_possession")
	self.light_operator.connect("dead", self, "_on_actor_death")

	self.light_operator.connect("actor_spotted", self, "_on_actor_spotted")
	self.light_operator.connect("actor_lost", self, "_on_actor_lost")

	self.light_operator.init($LightSpot)

	self.audience_suspicion_timer.connect("timeout", self, "_on_suspicious_timer_tick")
	self.audience_suspicion_timer.set_wait_time(1.0)
	self.audience_suspicion_timer.one_shot = false # I think this is default, but not sure. No time to test!
	self.audience_suspicion_timer.start()

	self.audience_excitement_timer.connect("timeout", self, "_on_excited_timer_tick")
	self.audience_excitement_timer.set_wait_time(1.0)
	self.audience_excitement_timer.one_shot = false # I think this is default, but not sure. No time to test!
	self.audience_excitement_timer.start()

	self.audience_suspicion_meter.min_value = 0
	self.audience_suspicion_meter.max_value = 100
	self.audience_suspicion_meter.value = 0

	self.audience_excitement_meter.min_value = 0
	self.audience_excitement_meter.max_value = 100
	self.audience_excitement_meter.value = 10

	self.end_game_timer.connect("timeout", self, "_on_game_end")
	self.end_game_timer.set_wait_time(60.0)
	self.end_game_timer.one_shot = true
	self.end_game_timer.start()


func _on_actor_possession() -> void:
	print("actor possessed")

	if is_instance_valid(self.actor_in_spotlight):
		self.actor_portrait_sprite.show()
		if self.actor_in_spotlight.name == "Beatrice":
			self.actor_portrait_sprite.frame_coords = Vector2(0, 1)
		elif self.actor_in_spotlight.name == "Dante":
			self.actor_portrait_sprite.frame_coords = Vector2(0, 4)

	remove_child(self.ghost)


func _on_actor_death(pos : Vector2) -> void:
	print("actor dropped dead at position ", pos)

	self.ghost.position = pos
	add_child(self.ghost)


func _on_actor_speaking(text : String, portrait_i : int, actor : Actor) -> void:
	if actor.name == "Beatrice":
		self.actor_portrait_sprite.frame_coords = Vector2(portrait_i, 1)
	elif actor.name == "Dante":
		# self.actor_portrait_sprite.frame_coords = Vector2(4, 3)
		self.actor_portrait_sprite.frame_coords = Vector2(portrait_i, 4)

	self.actor_portrait_sprite.show()

	self.dialogue_box.text = actor.name + ":\n" + text

	if actor == self.last_actor_to_speak:
		self.audience_excitement_meter.value += 3
	else:
		self.audience_excitement_meter.value += 15

	self.last_actor_to_speak = actor


func _on_actor_spotted(actor : Object) -> void:
	self.actor_in_spotlight = actor.owner as Actor

	if !is_instance_valid(self.actor_in_spotlight):
		return

	self.actor_portrait_sprite.show()
	if self.actor_in_spotlight.name == "Beatrice":
		self.actor_portrait_sprite.frame_coords = Vector2(0, 1)
	elif self.actor_in_spotlight.name == "Dante":
		self.actor_portrait_sprite.frame_coords = Vector2(0, 4)

	print(self.actor_in_spotlight.name, " in spotlight")


func _on_actor_lost() -> void:
	print(self.actor_in_spotlight.name, " lost")

	self.actor_in_spotlight = null


func _on_suspicious_timer_tick() -> void:
	if suspicious_stuff_going_on:
		self.audience_suspicion_meter.value += 10
	else:
		self.audience_suspicion_meter.value -= 5

	self.audience_suspicion_meter.value = max(0, self.audience_suspicion_meter.value)


func _on_excited_timer_tick() -> void:
	self.audience_excitement_meter.value -= 2
	self.audience_excitement_meter.value = max(0, self.audience_excitement_meter.value)


func _on_game_end() -> void:
	print("*applause*")

	self.block_input = true

	for actor in self.actors:
		actor.dialogue_option_panel.hide()

	self.end_game_animation_player.play("fade_to_black")
	self.end_game_animation_player.queue("fade_in_text")


func _input(_event: InputEvent) -> void:
	if self.block_input:
		get_tree().set_input_as_handled()


func _process(_delta: float) -> void:
	self.fps_display.text = String(Engine.get_frames_per_second())

	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()

	if !self.audience_has_reacted and self.audience_suspicion_meter.value >= 100:
		self.audience_has_reacted = true
		self.actor_portrait_sprite.show()
		self.actor_portrait_sprite.frame_coords = Vector2(0, 1)


		self.dialogue_box.text = "Whisper in the audience:\n Are they... Are they napping on stage?"


	if self.actor_in_spotlight == null:
		self.actor_in_spotlight_state_label.text = "no actor in spotlight"
		self.suspicious_stuff_going_on = false
		self.actor_portrait_sprite.frame_coords = Vector2(2, 0)
	else:
		self.actor_in_spotlight_state_label.text = "actor in spotlight is " + self.actor_in_spotlight.get_state_name()

		if self.actor_in_spotlight.get_state_i() == self.actor_in_spotlight.STATE_DEAD:	
			self.suspicious_stuff_going_on = true

			self.actor_portrait_sprite.show()
			if self.actor_in_spotlight.name == "Beatrice":
				self.actor_portrait_sprite.frame_coords = Vector2(0, 0)
			elif self.actor_in_spotlight.name == "Dante":
				self.actor_portrait_sprite.frame_coords = Vector2(0, 3)
		else:
			self.suspicious_stuff_going_on = false



func _exit_tree() -> void:
	# Have to manually free the ghost because it might not be in the tree currently and then
	# the states of its state machine would't get cleaned up if that's the case.
	self.ghost.free()


func get_dialogue_option_set() -> Array:
	var set : Array = self.dialogue_options[self.current_dialogue_option_set_i % self.dialogue_options.size()]
	self.current_dialogue_option_set_i += 1

	return set


func load_dialogue() -> Array:
	var _dialogue_options : Array = []
	var current_option_set : Array = []

	var regex : RegEx = RegEx.new()
	regex.compile("^(\\d+)###([^#]+)###(.+)")

	var f = File.new()
	f.open("res://dialogue.txt", File.READ)

	while not f.eof_reached():
		var line = f.get_line()

		var reg_match : RegExMatch = regex.search(line)
		# Sets of dialogue options are separated by empty lines.
		if reg_match == null:
			_dialogue_options.append(current_option_set)
			current_option_set = []
			continue

		var portrait_i = reg_match.get_string(1)
		var shortened_line = reg_match.get_string(2)		
		var cleaned_line = reg_match.get_string(3)		

		current_option_set.append([int(portrait_i), shortened_line, cleaned_line])

	# Append last remaining set
	if current_option_set.size() > 0:
		_dialogue_options.append(current_option_set)

	f.close()
	return _dialogue_options