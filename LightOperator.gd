extends Node2D

signal possessed
signal dead

signal actor_spotted
signal actor_lost

const STATE_DEAD : int = 0
const STATE_OPERATING_LIGHT : int = 1

var states : Array = [
	preload("res://states/dead_light_operator.gd").new(self),
	preload("res://states/operating_light.gd").new(self)
]

const SPEED_OF_LIGHT : int = 70

var spot : Position2D
var direction : Vector2 = Vector2.ZERO
var ghost_is_nearby : bool = false
var light_direction : Vector2 = Vector2.DOWN
var currently_spotted_actor : Object = null

onready var sprite : Sprite = $Sprite

onready var light : Node2D = $Light
onready var light_ray_cast : RayCast2D = $Light/LightRayCast


func init(_spot : Position2D) -> void:
	self.spot = _spot


func _ready() -> void:
	$StateMachine.init(self.states, STATE_DEAD, $StateDebugLabel)


func _physics_process(_delta: float) -> void:
	var light_vector : Vector2 = self.spot.global_position - self.light.global_position
	self.light.rotate(self.light_direction.angle_to(light_vector))
	self.light_direction = light_vector.normalized()

	self.light_ray_cast.cast_to = Vector2.DOWN * self.global_position.distance_to(self.spot.global_position)

	self.light_ray_cast.force_raycast_update()
	self.set_currently_spotted_actor(self.light_ray_cast.get_collider())


func set_ghost_is_nearby(is_nearby : bool) -> void:
	self.ghost_is_nearby = is_nearby


func set_currently_spotted_actor(actor : Object) -> void:
	if self.currently_spotted_actor == null:
		if actor != null:
			emit_signal("actor_spotted", actor)
	else:
		if actor == null:
			emit_signal("actor_lost")
		if actor != null and actor != self.currently_spotted_actor:
			emit_signal("actor_spotted", actor)

	self.currently_spotted_actor = actor


func signal_possession() -> void:
	emit_signal("possessed")


func signal_death() -> void:
	emit_signal("dead", self.global_position)
