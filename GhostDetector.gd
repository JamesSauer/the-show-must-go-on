extends Area2D

onready var parent : Node2D = get_owner()


func _ready() -> void:
	self.connect("area_entered", self, "_on_area_entered")
	self.connect("area_exited", self, "_on_area_exited")


func _on_area_entered(_ghost : Area2D) -> void:
	print("entered")

	if self.parent.has_method("set_ghost_is_nearby"):
		self.parent.set_ghost_is_nearby(true)


func _on_area_exited(_ghost : Area2D) -> void:
	print("exited")

	if self.parent.has_method("set_ghost_is_nearby"):
		self.parent.set_ghost_is_nearby(false)
